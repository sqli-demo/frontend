import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { SQLIDashboardComponent } from './dashboard/s-q-l-i-dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule} from "@angular/router";
import { NoSQLIDashboardComponent } from './no-sqlidashboard/no-sqlidashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    SQLIDashboardComponent,
    NoSQLIDashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
