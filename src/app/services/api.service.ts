import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Posts} from "../model/posts.model";

const AUTH_API = 'http://localhost:8080/api/';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  testConnection(): Observable<Object>{
    return this.http.get(AUTH_API+"login/test");
  }
/*
  login(): Promise<any> {
    return new Promise( resolve => {
      localStorage.setItem('loggedIn', 'true');
      resolve(true);
    });
    // return this.http.get(AUTH_API+"login/test");
  }

  isLoggedIn(): boolean{
    return !!localStorage.getItem('loggedIn');
  }*/

  loginInsecure(email: string, password: string): Observable<any>{
    return this.http.post(AUTH_API+"login/insecure", {email, password});
  }

  listPost(): Observable<any>{
    return this.http.get(AUTH_API+"post/list");
  }

  addPost(newPost: string): Observable<any> {
    return this.http.post(AUTH_API+"post/new", newPost);
  }

  loginSecure(email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API+"login/secure", {email, password});
  }

  addPostSecurely(newPost: string): Observable<any> {
    return this.http.post(AUTH_API+"post/securely/new", newPost);

  }
}
