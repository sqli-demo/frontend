import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Router, RouterModule, Routes} from "@angular/router";
import {SQLIDashboardComponent} from "./dashboard/s-q-l-i-dashboard.component";


const routes: Routes = [{
  path: 'dashboard', component: SQLIDashboardComponent
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
