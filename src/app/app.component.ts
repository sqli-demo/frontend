import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {ApiService} from "./services/api.service";
import {Users} from "./model/users.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'sqlidemo';
  loginForm: any ={
    email:null,
    password:null,
  };
  loginForm2: any ={
    email:null,
    password:null,
  };
  isSQLILoggedIn: boolean = false;
  invalidLogin: boolean = false;
  invalidLogin2: boolean = false;
  isNoSQLILoggedIn: boolean = false;
  errorMessage: any;


  constructor(private http: HttpClient,
              private router: Router,
              private apiService: ApiService ) { }

  test() {
    this.apiService.testConnection().subscribe(response => {
      console.log('Result: ', response);
      },error=> {
        console.log('Result: ', error);
      }
    );
  }

  onSubmitSQLI() {
    const {email, password} = this.loginForm;
    this.apiService.loginInsecure(email, password).subscribe((response: boolean) => {
        if (response){
          this.isSQLILoggedIn = true;
          console.log("Login Successful.")
        }
        else
          this.invalidLogin = true;
      },error=> console.error('Login failed', error)
    )
  }

  onSubmitNoSQLI() {
    const {email, password} = this.loginForm2;
    this.apiService.loginSecure(email, password).subscribe((response:Users) => {
        console.log('Login result: ', response.username);
        this.isNoSQLILoggedIn = true;
      },error=> {
        console.error('Login failed: ', error.error.message);
        this.invalidLogin2 = true;
        this.errorMessage = error.error.message;
      }
    )
  }
}
