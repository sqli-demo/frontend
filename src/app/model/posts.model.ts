export class Posts {
  public id: string;
  public postBy : string;
  public description : string;
  public createdDate : string;

  constructor(id: string, postBy: string, description: string, createdDate: string) {
    this.id = id;
    this.postBy = postBy;
    this.description = description;
    this.createdDate = createdDate;
  }
}
