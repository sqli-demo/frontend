export class Users {
  public id: string;
  public username : string;
  public email : string;
  public fullName : string;

  constructor(id: string, username: string, email: string, fullName: string) {
    this.id = id;
    this.username = username;
    this.email = email;
    this.fullName = fullName;
  }
}
