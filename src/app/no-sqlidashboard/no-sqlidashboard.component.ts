import { Component } from '@angular/core';
import {Posts} from "../model/posts.model";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-no-sqlidashboard',
  templateUrl: './no-sqlidashboard.component.html',
  styleUrls: ['./no-sqlidashboard.component.css']
})
export class NoSQLIDashboardComponent {
  posts: Posts[] = [];
  newPost: any;
  hasError: boolean=false;
  errorMessage: any;
  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.loadItems();
  }

  private loadItems() {
    this.apiService.listPost().subscribe((data: Posts[]) =>{
      // data.forEach(post =>{
      //   console.log(post.description);
      // });
      this.posts = data;
    }, err =>{
      console.log("Error in listing posts.")
    });
  }

  addPost() {
    this.apiService.addPostSecurely(this.newPost).subscribe((response: Posts)=>{
      console.log(response.description);
      if (response.description !== null)
        this.posts.push(response);
    }, err =>{
      console.log("Error in adding posts."+ err.error.message);
      this.hasError = true;
      this.errorMessage = err.error.message;
    });
  }

}
