import { Component } from '@angular/core';
import {Posts} from "../model/posts.model";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-sqlidashboard',
  templateUrl: './s-q-l-i-dashboard.component.html',
  styleUrls: ['./s-q-l-i-dashboard.component.css']
})
export class SQLIDashboardComponent {
  posts: Posts[] = [];
  newPost: any;
  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.loadItems();
  }

  private loadItems() {
    this.apiService.listPost().subscribe((data: Posts[]) =>{
      // data.forEach(post =>{
      //   console.log(post.description);
      // });
      this.posts = data;
    }, err =>{
        console.log("Error in listing posts.")
    });
  }

  addPost() {
    this.apiService.addPost(this.newPost).subscribe((response: Posts)=>{
      console.log(response.description);
      if (response.description !== null)
        this.posts.push(response);
    }, err =>{
      console.log("Error in adding posts."+ err.error.message)

    });
  }
}
